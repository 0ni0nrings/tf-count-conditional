terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = "~> 4.21.0"
    }
  }
}

provider "aws" {
    alias = "us-east-1"
    region = "us-east-1"
}

provider "aws" {
    alias = "ap-southeast-2"
    region = "ap-southeast-2"
}


variable "bucket_enabled" {
  type        = bool
  description = "Create a new Bucket?"
  default     = false
}

module "s3_bucket" {
    count = var.bucket_enabled == true ? 1 : 0
    source = "./s3-module"
    
    providers = {
        aws.config = aws.ap-southeast-2
    }
}