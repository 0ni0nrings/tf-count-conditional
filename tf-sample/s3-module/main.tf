terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.config]
      version               = "~> 4.21.0"
    }
  }
}

# provider "aws" {
#     alias = "us-east-1"
#     region = "us-east-1"
# }

# provider "aws" {
#     alias = "ap-southeast-2"
#     region = "ap-southeast-2"
# }

variable "bucket_enabled" {
  type        = bool
  description = "Create new bucket?"
  default     = false
}


resource "aws_s3_bucket" "b" {
  # count = var.bucket_enabled ? 1 : 0
  bucket = "my-tf-test-bucket-bow-wow"
  provider = aws.config
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}